	#!/bin/bash

echo "*** --- Costomização do PS1 --- ***"
echo ""
echo ""

echo "Escolha uma das opções: "
echo ""
echo "1- Modificar a cor pra Verde"
echo "2- Modificar a cor pra Ciano"
echo "3- Modificar as infos para data"
echo "4- Modificar as infos para o caminho atual"
echo "5- Para retornar as config padrão"
read numero

test $numero == 1 && PS1='\[\033[01;32m\]\u@\h:\w\$ '
test $numero == 2 && PS1='\[\033[01;36m\]\u@\h:\w\$ '
test $numero == 3 && PS1='\[\033[01;37m\]\t: '
test $numero == 4 && PS1='\[\033[01;32m\]\w: '
test $numero == 5 && PS1='\[\033[01;37m\]\u@\h:\w\$ '
