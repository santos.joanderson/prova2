#!/bin/bash

echo "Calculadora BASH"
echo ""

echo "Digite um numero que desejar fazer a operação: "
read numero1

echo "Digite a operação desejada  e que seja válida (+, -, *, /)"
read operacao
echo ""

echo "Digite o outro número: "
read numero2
echo ""

resultado=$(echo "$numero1 $operacao $numero2" | bc -l)

echo "Resultado: $resultado"
